package main

import
(
	"fmt"
	"github.com/fsnotify/fsnotify"
	"log"
	"sync"
	"os"
	"crypto/tls"
	irc "github.com/fluffle/goirc/client"
	"github.com/pkg/errors"
	"net/mail"
	"github.com/jhillyerd/go.enmime"
	"github.com/jordan-wright/email"
	"strings"
	"net/smtp"
	. "bitbucket.org/sjoerdtimmer/ircmail/storage"
)




func ReadMail(fileName string) (*MailEvent, error) {
	// start by opening file
	file, err := os.Open(fileName)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Failed to open mail file '%v'", fileName))
	}
	defer file.Close()

	// parse to net/mail object
	msg, err := mail.ReadMessage(file)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to parse email.")
	}

	return msg, nil
}



	// check content type
	// decode multipart mime
	// complain if there is no text/plain part
	// detect replies and signatures. Discard everything after the start of the signature/reply
	// split message by empty lines
	// each part becomes one irc message
func ProcessMail(msg MailEvent) error {
	// parse net/mail object
	mime, err := enmime.ParseMIMEBody(&msg)
	// check for problems
	if err != nil {
		return errors.Wrap(err, "Failed to parse mime email")
	}
	if mime.Text == "" {
		return errors.New("The email contained no plaintext part, either directly or in a nested multipart/mime message part.")
	}

	// determine sender end receiver raw addresses
	rawFrom, err := mail.ParseAddress(msg.Header.Get("From"))
	if err != nil {
		return errors.Wrap(err, "could not parse From address")
	}
	rawTo, err := mail.ParseAddress(msg.Header.Get("To"))
	if err != nil {
		return errors.Wrap(err, "could not parse To address")
	}
	
	// determine which conversation this is a reply to
	conversation, ok := ConversationsByMail[MailKey{rawFrom.Address, rawTo.Address}]
	if !ok {
		return errors.Wrap(err, "from/to pair is not known?")
	}

	// remember the subject line
	conversation.SubjectLine = msg.Header.Get("Subject")
	conversation.Reference   = msg.Header.Get("Message-ID")
		
	// filter reply
	normalizedText := strings.Replace(strings.Replace(mime.Text, "\r\n", "\n", -1), "\r", "\n", -1)
	// take the first paragraph
	replyText := strings.Split(normalizedText, "\n\n")[0]

	// send the message
	conversation.Connection.Conn.Privmsg(conversation.Channel, replyText)
	
	return nil
}

func ProcessIRC(msg IRCEvent) error {
	// lookup the conversation
	conversation, ok := ConversationsByIrc[IRCKey{msg.Connection, msg.Channel}]
	if !ok {
		return errors.New("A message was received in a channel that we didn't know you were in. Possibly this is a private message?")
	}

	log.Printf("using saved Subject: %v", conversation.SubjectLine)
	
	// send email:
	email := email.NewEmail()
	email.From    = fmt.Sprintf("%s in %s <%s>", msg.From, msg.Channel, conversation.SystemMail)
	email.To      = []string{conversation.UserMail}
	email.Subject = conversation.SubjectLine
	email.Headers.Add("References", conversation.Reference)
	email.Headers.Add("In-Reply-To", conversation.Reference)
	email.Text    = []byte(msg.Message)
	err := email.Send("localhost:25", smtp.PlainAuth("", "", "", "")) 
	if err != nil {
		return errors.Wrap(err, "failed to send mail")
	}
	
	return nil
}

func MailLoop() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	err = watcher.Add("/home/sjoerd/Maildir/new")
	if err != nil {
		log.Fatal(err)
	}

	for/*ever*/ {
		select {
		case event := <-watcher.Events:
			log.Println("event:", event)
			switch event.Op {
			case fsnotify.Create:
				log.Println("file created: ",event.Name)
				mailEvent, err := ReadMail(event.Name)
				if err != nil {
					log.Print(err)
					continue
				}
				inmail <- *mailEvent
			case fsnotify.Write:
				log.Println("file written:",event.Name)
			case fsnotify.Remove:
				log.Println("file removed:",event.Name)
			case fsnotify.Rename:
				log.Println("file renamed:",event.Name)
			case fsnotify.Chmod:
				log.Println("file mode changed:",event.Name)
			}
		case err := <-watcher.Errors:
			log.Println("error:", err)
		}
	}
}

func IRCLoop() {
	irccfg := irc.NewConfig("bot-945")
	irccfg.SSL = true
	irccfg.SSLConfig = &tls.Config{ServerName: "chat.freenode.net"}
	irccfg.Server = "chat.freenode.net:7000"
	irccfg.NewNick = func(takennick string) string { return takennick + "_" }

	ircclient := irc.Client(irccfg)
	var wg sync.WaitGroup
	wg.Add(1)
	ircconn := Connection{*ircclient, wg}


	ircclient.HandleFunc(irc.CONNECTED, func(conn *irc.Conn, line *irc.Line) {
		log.Print("ircclient connected")
		conn.Join("#botzone")
	})

	ircclient.HandleFunc(irc.DISCONNECTED, func(conn *irc.Conn, line *irc.Line) {
		log.Print("ircclient disconnected")
	})

	ircclient.HandleFunc(irc.PRIVMSG, func(conn *irc.Conn, line *irc.Line) {
		//log.Print("irc message received: "+line.Raw)
		inirc <- IRCEvent {
			&ircconn,
			line.Target(),
			line.Cmd,
			line.Nick,
			line.Text() }
	})

	if err := ircclient.Connect(); err != nil {
		log.Print("Connection error: %s", err.Error())
	}

	// build data structure
	testconv := Conversation {
		"sjoerd.timmer@gmail.com",
		"test@ircmail.sttim.nl",
		"The previous subject line",
		"", // reference used to make google understand which thread this is
		IRCConf {
			"ssl.irc.atw-inter.net",
			6697,
			true,
			"bot-945",
			"",
		},
		"#botzone",
		"",
		&ircconn,
	}

	ConversationsByMail[MailKey{"sjoerd.timmer@gmail.com","test@ircmail.sttim.nl"}] = &testconv;
	ConversationsByIrc[IRCKey{&ircconn,"#botzone"}] = &testconv;


	// wait for client to finish
	log.Print("irc client going to wait now")
	wg.Wait()
	ircclient.Close()
	log.Print("irc client closed")
}


// globals include 2 channels and to lookup maps
var inmail = make(chan MailEvent)
var inirc  = make(chan IRCEvent)

func main() {
	fmt.Printf("IRC-Mail bridge")

	InitStorage()
	log.Print("storage loaded")
	
	// start receiving emails
	go MailLoop()

	// start irc connections
	go IRCLoop()


	for/*ever*/ {
		select {
		case msg := <-inmail:
			log.Printf("email received: %+v", msg)
			ProcessMail(msg)
		case msg := <-inirc:
			log.Printf("irc message received: %v %v %v %v",msg.Channel, msg.Type, msg.From, msg.Message)
			ProcessIRC(msg)
		}
	}

}
