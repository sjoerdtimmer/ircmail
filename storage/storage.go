package storage

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/pkg/errors"
	"os"
	"os/signal"
	"sync"
	"net/mail"
	"log"
	irc "github.com/fluffle/goirc/client"
)

type IRCConf struct {
	Server		string
	Port		int
	UseSSL		bool
	Nick		string
	NickPwd		string
}

type Connection struct {
	Conn		irc.Conn
	WaitGroup	sync.WaitGroup
}

type Conversation struct {
	// members relating to email side
	UserMail	string
	SystemMail	string
	SubjectLine	string
	Reference   string
	// binding element
	Settings	IRCConf    // 1-1 because settings may be changed
	// members relating to irc side
	Channel		string
	ChannelPwd	string
	Connection	*Connection `gorm:"-"`
}

// when receiving mail a lookup is necesary 
// from from+to pair to Conversation
type MailKey struct {
	UserMail	string
	SystemMail	string
}
// when receiving irc messages a lookup is necesary
// from client+channel to Conversation
type IRCKey struct {
	Connection	*Connection
	Channel		string
}

// types for incomming message evens
type MailEvent = mail.Message
	
type IRCEvent	struct {
	Connection	*Connection
	Channel		string
	Type		string
	From		string
	Message		string
}


var ConversationsByMail = make(map[MailKey]*Conversation)
var ConversationsByIrc  = make(map[IRCKey]*Conversation)


func InitStorage() {
	// TODO: make this configurable
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		log.Fatal(errors.Wrap(err, "Failed to initiate gorm database"))
	}

	// migrate if necessary:
	db.AutoMigrate(&Conversation{})
	db.AutoMigrate(&IRCConf{})

	// load conversations:
	var allConversations []Conversation
	db.Find(&allConversations)

	log.Printf("%d conversations loaded from the database", len(allConversations))
	for _,c := range allConversations {
		log.Printf("Conversation: %+v", c)
	}
	
	testconv := Conversation {
		"sjoerd.timmer@gmail.com",
		"test@ircmail.sttim.nl",
		"", // subject
		"", // reference
		IRCConf {
			"ssl.irc.atw-inter.net",
			6697,
			true,
			"bot-945",
			"",
		},
		"#botzone",
		"",
		nil,
	}

	db.Create(&testconv)


	
	// wait for interrupt and do cleanup
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c) // any signal
		// wait for signal:
		s := <- c
		// do cleanup:
		log.Printf("signal %+v received cleaning up db connection...", s)
		db.Close()
		log.Print("db connection closed")
	}()
}



func AddConversation(c Conversation) {

}

func DelConversation(c Conversation) {

}

func UpdateConversation(old, new Conversation) {

}
